#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <getopt.h>
#include <unistd.h>
#include "../include/funciones.h"

double **crear_matriz(unsigned long filas, unsigned long columnas){

	double **matriz = (double **)calloc(filas, sizeof(double *));

	if(matriz == NULL){
		return NULL;	
	}

	unsigned long i = 0;
	for(i = 0; i < filas; i++){
		matriz[i] = calloc(columnas, sizeof(double));
		if(matriz[i]==NULL){
			return NULL;
		}	
	}

	return matriz;
}


void liberar_matriz(double **m, unsigned long filas, unsigned long columnas){
	for(unsigned long i = 0; i < filas; i++){
		free(m[i]);
		m[i]=NULL;	
	}
	free(m);
	m=NULL;
}


void llenar_matriz_azar(double **m, unsigned long filas, unsigned long columnas, float max){
	srand( time( NULL ) );
	for(unsigned long i = 0; i < filas; i++){
		for(unsigned long j = 0; j < columnas; j++){
			m[i][j] = ((double)rand()/(double)(RAND_MAX)) * max;
		}	
	}

}



void mostrar_matriz(double **m, unsigned long filas, unsigned long columnas){
	
	for(unsigned long i = 0; i < filas; i++){
		for(unsigned long j = 0; j < columnas; j++){
			printf("%6.2f ", m[i][j]); 
		}
		printf("\n");	
	}
	printf("\n");

}



//Calcula el producto punto de una fila de m1, con una columna de m2
double producto_punto(double **m1, unsigned long filasM1, unsigned long columnasM1, unsigned long fila,
	             double **m2, unsigned long filasM2, unsigned long columnasM2, unsigned long columna){

	double resultado = 0.0f;
	//printf("fila %ld columna %ld\n", fila, columna);
	//columnas m1
	for(unsigned long j = 0; j < columnasM1; j++){
		//filas m2
		for(unsigned long i = 0; i < filasM2; i++){
		//	printf("m1[fila][j] = %f, m2[i][columna] = %f\n", m1[fila][j], m2[i][columna]);
			resultado += (m1[fila][j] * m2[i][columna]);
			j++;
		}	
	}
//	printf("%f\n", resultado);
	return resultado;
}




void multiplicar_matrices(double **m1, unsigned long filasM1, unsigned long columnasM1,
			  double **m2, unsigned long filasM2, unsigned long columnasM2,
			  double **res, unsigned long filasRes, unsigned long columnasRes, int nHilos){
	pthread_t threads[nHilos];
	for(int i = 0;i<nHilos;i++){
		WorkData *w = malloc(sizeof(WorkData));
		w->f1=filasM1;
		w->f2=filasM2;
		w->f3=filasRes;
		w->c1=columnasM1;
		w->c2=columnasM2;
		w->c3=columnasRes;
		w->m1=m1;
		w->m2=m2;
		w->m3=res;
		w->id=i;
		w->nHilos=nHilos;
		pthread_create(&(threads[i]),NULL,thread,w);
	}
	for(int i = 0; i<nHilos;i++){
		pthread_join(threads[i],NULL);
	}
	
}


void *thread(void *arg){
	WorkData *w= (WorkData *)arg;
	for(unsigned long filres = w->id; filres < w->f3; filres+=w->nHilos){
		for(unsigned long colres = 0; colres < w->c3; colres++){
			w->m3[filres][colres] = producto_punto(w->m1, w->f1, w->c1, filres,
							     w->m2, w->f2, w->c2, colres);
		}
	}
	free(w);
	return NULL;
}

double obtener_tiempo(){
	struct timespec tsp;
	
	clock_gettime(CLOCK_REALTIME, &tsp);
	double secs = (double)tsp.tv_sec;
	double nsecs = (double)tsp.tv_nsec / 1000000000.0f;

	return secs + nsecs;

}

