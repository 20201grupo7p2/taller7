matmul: obj/matriz.o obj/funciones.o
	gcc -Wall $^ -o $@ -pthread

obj/matriz.o:src/matriz.c
	gcc -Wall -c src/matriz.c -o $@

obj/funciones.o:src/funciones.c
	gcc -Wall -c src/funciones.c -L include/ -o $@

.PHONY: clean
clean:
	rm -rf bin obj matmul


