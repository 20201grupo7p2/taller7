#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

 typedef struct _m {
 	double **m1;
 	double **m2;
 	double **m3;
 	unsigned long f1;
 	unsigned long f2;
 	unsigned long f3;
 	unsigned long c1;
 	unsigned long c2;
 	unsigned long c3;
 	int id;
 	int nHilos;
 } WorkData;
void liberar_matriz(double **m, unsigned long filas, unsigned long columnas);


void llenar_matriz_azar(double **m, unsigned long filas, unsigned long columnas, float max);


double **crear_matriz(unsigned long filas, unsigned long columnas);

void mostrar_matriz(double **m, unsigned long filas, unsigned long columnas);

double producto_punto(double **m1, unsigned long filasM1, unsigned long columnasM1, unsigned long fila,
	             double **m2, unsigned long filasM2, unsigned long columnasM2, unsigned long columna);


void multiplicar_matrices(double **m1, unsigned long filasM1, unsigned long columnasM1,
			  double **m2, unsigned long filasM2, unsigned long columnasM2,
			  double **res, unsigned long filasRes, unsigned long columnasRes, int nHilos);

double obtener_tiempo();
void *thread(void *arg);

